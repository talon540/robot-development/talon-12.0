/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

// Import Packages
import edu.wpi.first.wpilibj.ADXRS450_Gyro;
import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.Solenoid;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import org.opencv.core.Mat;

import com.ctre.phoenix.motorcontrol.ControlMode;
//import edu.wpi.first.wpilibj.PWM;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.DigitalSource;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.cscore.UsbCamera;
import edu.wpi.cscore.VideoMode;
import edu.wpi.first.networktables.NetworkTable;
//import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the IterativeRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.properties file in the
 * project.
 */

// TIMESTAMP January 30 (Prototype 12.0)

public class Robot extends TimedRobot {
	private static final String kDefaultAuto = "Default";
	private static final String kCustomAuto = "My Auto";

	private SendableChooser<String> m_chooser = new SendableChooser<>();

	ADXRS450_Gyro gyro;//, gyro1; // Specific gyroscope that is used in 11.0
	Encoder enc1, enc2, enc3, wristEnc;
	AnalogInput infaredsensor;
	AnalogInput colorsensor1, colorsensor2, colorsensor3;
	DigitalSource liDARdio;
	double angle, angle1, pulse, dist, irdist, speed, color1, color2, color3, distancelidar;
	boolean height, arm, hand;
	double angleTarget;
	double dist2, pulse2, dist3, pulse3;
	double wristDist, wristPulse;
	double elevatorSpeed;
	LiDARSensor liDAR;
	TalonSRX frontRight, middleRight, backRight, frontLeft, middleLeft, backLeft;
	TalonSRX intakeRoller;
	TalonSRX cargolift;
	TalonSRX hatchwrist;
	DoubleSolenoid hatchintakearm, hatchintakehand, climbfront, climbback;
	PIDController test;
	AnalogInput ir;
	double irDist;
	boolean irVal;
	// Controllers
	Joystick leftJoy, rightJoy;
	XboxController xbox;
	double left, right; // XBox Joy Stick direction

	// Elevator macro variables
	int selectedVal;
	double[] heights;

	int counter;
	
	//NetworkTables
	NetworkTableInstance inst;
	NetworkTable table;

	double camDist, camProp, camAngleProp, camAngle;
	String camAlign, camAngleAlign;

	//Camera tracking macro values
	boolean camTrack;
	int camCounter;
	double camTargetDist, camTargetAngle, camTurnAngle, triangleDist;
	String camTargetAlign;

	// compressor
	Compressor compressor;
	boolean pressureSwitch;

	boolean following;

	boolean elevatorUp;

	//hatch arm toggle
	boolean hatchtoggle;
	// UNKNOWN CONSTANTS - fill when testing
	private static final double ELEVATOR_SINGLE_LEVEL = 0;
	private static final double ELEVATOR_SECOND_LEVEL = 0;
	private static final double BALL_BOTTOM_LEVEL = 0;
	private static final double HATCH_BOTTOM_LEVEL = 0;
	private static final double ELEVATOR_SPEED = 0;
	private static final double MAX_ELEVATOR_ACCEL = 0.04;
	private static final double CARGO_LEVEL = 0;
	private static final double COLOR_TAPE_VALUE = 0.5;
	private static final double MOTOR_SPEED_TAPE = 0.5; // should not be negative in final
	//private static final double ERROR = 3;
	private static final double TURNING_SPEED = 0.4; // should not be negative in final
	private static final double DRIVE_CONSTANT  = -0.8;

	@Override
	public void robotInit() {
		m_chooser.addDefault("Default Auto", kDefaultAuto);
		m_chooser.addObject("My Auto", kCustomAuto);
		SmartDashboard.putData("Auto choices", m_chooser);

		// Initializes gyroscope + encoder
		gyro = new ADXRS450_Gyro();
		//gyro1 = new ADXRS450_Gyro();
		enc1 = new Encoder(0, 1, true); // right side - backwards
		enc2 = new Encoder(2, 3, false); // left side
		enc3 = new Encoder(4, 5, false);
		wristEnc = new Encoder(6, 7, true);
		ir = new AnalogInput(3);
		
		intakeRoller = new TalonSRX(2);
		frontRight = new TalonSRX(7);
		//middleRight = new TalonSRX(8);
		backRight = new TalonSRX(9);
		//frontLeft = new TalonSRX(3);
		middleLeft = new TalonSRX(4);
		backLeft = new TalonSRX(5);

		// Encoder Specifications
		enc1.setMaxPeriod(0.1); // Maximum time after which encoder stops
		enc1.setMinRate(5);
		enc1.setDistancePerPulse(0.00078487); // Multiplier for distance traveled
												// //subject to change
		enc2.setMaxPeriod(0.1); // Maximum time after which encoder stops
		enc2.setMinRate(5);
		enc2.setDistancePerPulse(0.00078487); // Multiplier for distance
												// traveled //subject to change
		enc3.setMaxPeriod(0.1); // Maximum time after which encoder stops
		enc3.setMinRate(5);
		enc3.setDistancePerPulse(0.00078487); // Multiplier for distance
												// traveled //subject to change
		
		// Not going to work until mechanical/electrical fix the encoder mounting
		wristEnc.setMaxPeriod(0.1);
		wristEnc.setMinRate(5);
		wristEnc.setDistancePerPulse(1); // TODO: find the hecking number for degrees

		cargolift = new TalonSRX(6);
		// Initializes XBox Controller
		xbox = new XboxController(2);
		leftJoy = new Joystick(0);
		rightJoy = new Joystick(1);
		

		// other sensor
		infaredsensor = new AnalogInput(2);
		colorsensor1 = new AnalogInput(1);
		colorsensor2 = new AnalogInput(0);
		//colorsensor3 = new AnalogInput(3);
       	liDARdio = new DigitalInput(9);
       	liDAR = new LiDARSensor(liDARdio);
		irdist = ir.getVoltage();
		hatchwrist = new TalonSRX(1);
		hatchintakearm = new DoubleSolenoid(6, 7);
		hatchintakehand = new DoubleSolenoid(4, 5);
		climbfront = new DoubleSolenoid(2, 3);
		climbback = new DoubleSolenoid(0, 1);

		cargolift.enableCurrentLimit(false);

		counter = 0;

		//NetworkTables
		inst = NetworkTableInstance.getDefault();
		table = inst.getTable("/SmartDashboard");

		//POV camera
		//new Thread(() -> {
		UsbCamera camera = CameraServer.getInstance().startAutomaticCapture();
		//camera.setResolution(320, 260);
		//camera.setFPS(15);
		camera.setVideoMode(VideoMode.PixelFormat.kMJPEG, 320, 240, 24);
		//camera.setResolution(352, 288);
		camera.setExposureManual(60);
		camera.setWhiteBalanceManual(50);

		//Initializes camera tracking macro values
		camTrack = false;
		camCounter = 0;
		camTargetAlign = "N";
		camTargetAngle = 0;
		camTargetDist = 0;
		triangleDist = 0;
		camTurnAngle = 0;

		// elevator macro variables
		selectedVal = 0;
		heights = new double[]{0, 4.6, 8};

		following = false;

		elevatorSpeed = 0.0;

		compressor = new Compressor(0);
		pressureSwitch = false;
		compressor.setClosedLoopControl(true);

       	gyro.reset();
	}

	/**
	 * This function is called periodically during operator control.
	 */
	@Override

	// Controlled driving
	public void teleopPeriodic() {
		// Defines every variable
	  	angle = gyro.getAngle();
		//angle1 = gyro1.getAngle();
		dist = enc1.getDistance();
		pulse = enc1.getRaw();
		dist2 = enc2.getDistance();
		pulse2 = enc2.getRaw();
		dist3 = enc3.getDistance();
		pulse3 = enc3.getRaw();
		wristDist = wristEnc.getDistance();
		wristPulse = wristEnc.getRaw();
		irdist = ir.getVoltage();
		distancelidar = liDAR.getDistance();
		color1 = colorsensor1.getVoltage();
		color2 = colorsensor2.getVoltage();
		//color3 = colorsensor3.getVoltage();
		distancelidar = liDAR.getDistance();
		irDist = ir.getVoltage();
		irVal = irDist > 0.5;
		pressureSwitch = compressor.getPressureSwitchValue();
		//arm = hatchintakearm.get();
		//hand = hatchintakehand.get();

		//NetworkTable Values
		camAlign = table.getEntry("linearAlign").getString("N");
		camAngle = table.getEntry("angle").getDouble(0);


		// LiDAR specifications
		// Displays values on the SmartBoard
		SmartDashboard.putNumber("Angle: ", angle);
		//SmartDashboard.putNumber("Angle1: ", angle1);
		SmartDashboard.putNumber("Distance: ", dist);
		SmartDashboard.putNumber("Distance2: ", dist2);
		SmartDashboard.putNumber("Distance3: ", dist3);
		SmartDashboard.putNumber("Selected Height: ", selectedVal);
		SmartDashboard.putNumber("Wrist Angle: ", wristDist);
		SmartDashboard.putNumber("Pulse: ", pulse);
		SmartDashboard.putNumber("Pulse 2: ", pulse2);
		SmartDashboard.putNumber("Pulse 3: ", pulse3);
		SmartDashboard.putNumber("Wrist Pulse: ", wristPulse);
		SmartDashboard.putNumber("Infared Distance", irdist);
		SmartDashboard.putNumber("Color1", color1);
		SmartDashboard.putNumber("Color2", color2);
		SmartDashboard.putNumber("Color3", color3);
		SmartDashboard.putNumber("Distance_LiDAR", distancelidar);
		SmartDashboard.putBoolean("Hatch Arm", arm);
		SmartDashboard.putBoolean("Hatch Hand", hand);
		SmartDashboard.putBoolean("Pressure Switch Value:", pressureSwitch);
		boolean colorTape = color1 < COLOR_TAPE_VALUE || color2 < COLOR_TAPE_VALUE;
		SmartDashboard.putBoolean("Color Tape Macro Status:", colorTape);
		SmartDashboard.putNumber("IR Value:", irDist);
		SmartDashboard.putBoolean("Ball in intake:", irVal);
		
		//Camera Tracking values
		SmartDashboard.putString("Offset from Goal", camAlign);
		SmartDashboard.putNumber("Alignment Proportion", camProp);
		SmartDashboard.putNumber("Camera Macro Counter", camCounter);
		SmartDashboard.putBoolean("Camera Macro Enable", camTrack);
		SmartDashboard.putString("Angle Offset", camAlign);
		SmartDashboard.putNumber("Angle Prop", camAngleProp);

		// Controller Input --> Output
		drive();
		elevator();
		hatchWrist();
		hatch();
		intakeball();
		//climbing();
		
		if (leftJoy.getRawButtonPressed(8)) {
			wristEnc.reset();
		}

		if (leftJoy.getRawButtonPressed(10)) {
			gyro.reset();
		}

		if (leftJoy.getRawButton(1)) {
			setMotors(1, 1);
			intakeRoller.set(ControlMode.PercentOutput, -0.3);
		}

		if (colorTape) {
			if (rightJoy.getRawButton(1)) {
				followLine();
			}
		}

		//Checks joystick press for macro enabling
		/*if(leftJoy.getRawButtonPressed(1)) {
			if (camTrack == false) {
				camTrack = true;
			}
			else {
				camTrack = false;
			}
		}
		
		if (camTrack) {
			if (Math.abs(leftJoy.getY()) > 0.2 || Math.abs(rightJoy.getY()) > 0.2) {
				camTrack = false;
				camTargetAlign = "N";
				camTargetDist = 0;
				camTargetAngle = 0;
				triangleDist = 0;
				camTurnAngle = 0;
				camCounter = 0;
			}
			else {
				camMacroColorSensor();
			}
		}*/

	}

	// Drive Method
	public void drive() {
		// Defines right and left (NOTE: joystick y axis have positive as down, and negative as up, so we negate them)
		right = -rightJoy.getY();
		left = -leftJoy.getY();

		// Takes Absolute Value of left and ignores if it is less than 0.2
		if (Math.abs(left) < 0.2) {
			left = 0;
		}

		// Takes Absolute Value of right and ignores if it is less than 0.2
		if (Math.abs(right) < 0.2) {
			right = 0;
		}

		// if (colorsensor1.getVoltage() == 0 & colorsensor2.getVoltage()==0 &
		// colorsensor3.getVoltage() == 0) {

		// }

		// if (liDAR.getSpeed()>5) {
		// setMotors(left-0.5, right+0.5);
		// }
		setMotors(left, right);
		/*if (color1 < COLOR_TAPE_VALUE || color2 < COLOR_TAPE_VALUE){
			followLine();
		}*/
		
	}

	public void intakeball() {
		// Defines which inputs are used
		double xboxRight = xbox.getRawAxis(5);

		// Ignores if value is too small

		if (Math.abs(xboxRight) < 0.2) {
			xboxRight = 0;
		}
		if (xbox.getRawAxis(5) > 0.2) {
			intakeRoller.set(ControlMode.PercentOutput, -xbox.getRawAxis(5));
		} else if (xbox.getRawAxis(5) < -0.2) {
			intakeRoller.set(ControlMode.PercentOutput, -xbox.getRawAxis(5));
		} else {
			intakeRoller.set(ControlMode.PercentOutput, 0);
		}
	}

	public void elevator() {
		double targetSpeed = 0;
		// Up 1 level
		/*if (xbox.getYButton() == true) {
			if (dist3 - ELEVATOR_SECOND_LEVEL > 0) {
				cargolift.set(ControlMode.PercentOutput, -ELEVATOR_SPEED);
				// enc3.reset();
			}else if (dist3 - ELEVATOR_SECOND_LEVEL < 0) {
				cargolift.set(ControlMode.PercentOutput, ELEVATOR_SPEED);
				// enc3.reset();
			} else {
				cargolift.set(ControlMode.PercentOutput, 0);
			} // subject to change

		}*/
		// 1st level ball
		/*if (xbox.getYButton() == true && dist3 > ELEVATOR_SINGLE_LEVEL) {
			if (dist3 <= BALL_BOTTOM_LEVEL) {
				cargolift.set(ControlMode.PercentOutput, 0);
				enc3.reset();
			} else {
				cargolift.set(ControlMode.PercentOutput, -ELEVATOR_SPEED);
			}

		}*/

		/*// 1st level hatch
		if (xbox.getXButton() == true && dist3 > HATCH_BOTTOM_LEVEL) {
			if (dist3 <= HATCH_BOTTOM_LEVEL) {
				cargolift.set(ControlMode.PercentOutput, 0);
				enc3.reset();
			} else {
				cargolift.set(ControlMode.PercentOutput, -ELEVATOR_SPEED);
			}

		}

		// Cargo level
		if (xbox.getXButton() == true) {
			if (xbox.getXButton() == true && dist3 > CARGO_LEVEL) {
				cargolift.set(ControlMode.PercentOutput, -ELEVATOR_SPEED);
				if (dist3 == HATCH_BOTTOM_LEVEL) {
					cargolift.set(ControlMode.PercentOutput, 0);
				}
			}
		}*/
		
		// Left Trigger (UP)
		if (xbox.getRawAxis(2) > 0.1) {
			/*if (Math.abs(xbox.getRawAxis(3)-elevatorSpeed) > MAX_ELEVATOR_ACCEL) {
				if (xbox.getRawAxis(3) > elevatorSpeed) {
					elevatorSpeed += MAX_ELEVATOR_ACCEL;
				} else {
					elevatorSpeed -= MAX_ELEVATOR_ACCEL;
				}
			} else {
				if (xbox.getRawAxis(3) > elevatorSpeed) {
					elevatorSpeed += MAX_ELEVATOR_ACCEL;
				} else {
					elevatorSpeed -= MAX_ELEVATOR_ACCEL;
				}
			}*/
			elevatorUp = true;
			cargolift.set(ControlMode.PercentOutput, -xbox.getRawAxis(2));
			//cargolift.set(ControlMode.PercentOutput, -elevatorSpeed);
		}
		// Right Trigger (DOWN)
		else if (xbox.getRawAxis(3) > 0.1) {
			/*if (Math.abs(xbox.getRawAxis(2)-elevatorSpeed) > MAX_ELEVATOR_ACCEL) {
				if (xbox.getRawAxis(2) > elevatorSpeed) {
					elevatorSpeed += MAX_ELEVATOR_ACCEL;
				} else {
					elevatorSpeed -= MAX_ELEVATOR_ACCEL;
				}
			} else {
				if (xbox.getRawAxis(2) > elevatorSpeed) {
					elevatorSpeed += MAX_ELEVATOR_ACCEL;
				} else {
					elevatorSpeed -= MAX_ELEVATOR_ACCEL;
				}
			}*/
			elevatorUp = false;
			cargolift.set(ControlMode.PercentOutput, -0.1);
		} else if (dist3 > 0.8) {
			elevatorUp = false;
			cargolift.set(ControlMode.PercentOutput, -0.43); // 0.48 for comp
		} else {
			elevatorUp = false;
			cargolift.set(ControlMode.PercentOutput, 0);
		}

		if (xbox.getAButton() == true) {
			if (dist3 < 3.43) {
				cargolift.set(ControlMode.PercentOutput, -0.9);
			} else if (dist3 > 3.45) {
				cargolift.set(ControlMode.PercentOutput, -0.1);
			} else {
				cargolift.set(ControlMode.PercentOutput, -0.45);
			}
		}
	}

	// right = xbox.getX(1.0);
	// left = xbox.getY(2);

	// Takes Absolute Value of left and ignores if it is less than 0.2
	// if (Math.abs(left) < 0.2) {
	// left = 0;
	// }

	// Takes Absolute Value of right and ignores if it is less than 0.2
	// if (Math.abs(right) < 0.2) {
	// right = 0;
	// }
	// if (Math.abs(xbox.getRawAxis(1)) > 0.2) {
	// intakeL = xbox.getRawAxis(1);
	// xb

	// }
	// else {
	// intakeL = 0;
	// }

	// if (Math.abs(xbox.getRawAxis(5)) > 0.2) {
	// intakeR = xbox.getRawAxis(5);
	// }
	// else {
	// intakeR = 0;
	// }

	// intakesetMotors(intakeL, -intakeR);
	// }*/

	public void hatchWrist() {
		// if (xbox.getRawButton(5) == true && wristDist <= 100) {
		if (xbox.getRawButton(5) == true) {
			if (irVal && elevatorUp) {
				intakeRoller.set(ControlMode.PercentOutput, -0.2);
				hatchwrist.set(ControlMode.PercentOutput, 0.6);
			} else {
				intakeRoller.set(ControlMode.PercentOutput, -0.2);
				hatchwrist.set(ControlMode.PercentOutput, 0.5);
			}
		} // else if (xbox.getRawButton(6) == true && wristDist >= 0) {
		else if (xbox.getRawButton(6) == true) {
			hatchwrist.set(ControlMode.PercentOutput, -0.3);
		} else if (!elevatorUp && !irVal) {
			hatchwrist.set(ControlMode.PercentOutput, 0);
		}

	}

	public void hatch() {
		// Defines which inputs are used
		double xboxLeft = xbox.getRawAxis(2);
		// Ignores if value is too small
		if (Math.abs(xboxLeft) < 0.2) {
			xboxLeft = 0;
		}

		/*
		 * outtake hatch = intakearm forward
		 * intake hatch = hatchintakehand reverse
		 * push bar in = intakehand forward
		 * push bar out = hatchintakearm reverse
		*/
		if (xbox.getYButton()==true) { // bar in
			hatchintakearm.set(DoubleSolenoid.Value.kForward);
		} else if (xbox.getBButton()==true) { // bar out
			hatchintakearm.set(DoubleSolenoid.Value.kReverse);
		} else if (xbox.getRawAxis(1) > 0.4) {
			hatchintakehand.set(DoubleSolenoid.Value.kReverse);
		} else if (xbox.getRawAxis(1) < -0.4) {
			hatchintakehand.set(DoubleSolenoid.Value.kForward);
		} else {
			hatchintakehand.set(DoubleSolenoid.Value.kOff);
			hatchintakearm.set(DoubleSolenoid.Value.kOff);
		}
	}

	public void camMacroColorSensor() {
		if (camCounter == 0) {
			camTargetAlign = camAlign;
			gyro.reset();
			enc1.reset();	
			camCounter++;
		}
		else if (camCounter == 1) {
			if (camTargetAlign.equals("C")) {
				camTargetDist = distancelidar;
				camTargetAngle = camAngle;
				camCounter++;
			}
			else if (camTargetAlign.equals("L")) {
				if (camAlign.equals("C")) {
					setMotors(0, 0);
					camTargetDist = distancelidar;
					camTargetAngle = camAngle;
					camCounter++;
				}
				else {
					setMotors(-0.2, 0.2);
				}
			}
			else if (camTargetAlign.equals("R")) {
				if (camAlign.equals("C")) {
					setMotors(0, 0);
					camTargetDist = distancelidar;
					camTargetAngle = camAngle;
					camCounter++;
				}
				else {
					setMotors(0.2, -0.2);
				}
			}
			else {
				camCounter+= 5;
			}
		}
		else if (camCounter == 2) {
			camTargetAngle = Math.toRadians(camTargetAngle);
			triangleDist = Math.sqrt(Math.pow(camTargetDist, 2) + (18^2) - (2 * 18 * camTargetDist * Math.cos(Math.abs(camTargetAngle))));
			camTurnAngle = Math.toDegrees(Math.asin((18 * Math.sin(Math.abs(camTargetAngle)))/triangleDist));
			camCounter++;
		}
		else if (camCounter == 3) {
			if (camTargetAngle > 0) {
				turnLeft(camTurnAngle, camCounter);
			} else if (camTargetAngle < 0) {
				turnRight(camTurnAngle, camCounter);
			}
		}
		else if (camCounter == 4) {
			moveForward(triangleDist, camCounter);
		}
		else if (camCounter == 5) {
			if (color1 < COLOR_TAPE_VALUE || color2 < COLOR_TAPE_VALUE){
				if (distancelidar > 6) {
					followLine();
				} else {
					camCounter++;
				}
			} 
		}
		else if (camCounter == 6) {
			setMotors(0, 0);
			camTargetAlign = "N";
			camTargetDist = 0;
			camTargetAngle = 0;
			triangleDist = 0;
			camTurnAngle = 0;
			camCounter = 0;
			camTrack = false;
		}
	}

	public void setMotors(double left, double right) {
		// Specifies direction of motors
		frontRight.set(ControlMode.PercentOutput, -right);
		//middleRight.set(ControlMode.PercentOutput, -right);
		backRight.set(ControlMode.PercentOutput, -right);
		//frontLeft.set(ControlMode.PercentOutput, left);
		middleLeft.set(ControlMode.PercentOutput, left);
		backLeft.set(ControlMode.PercentOutput, left);
	}

	/*public void climbing() {
		if (leftJoy.getRawButton(5)) {
			climbback.set(DoubleSolenoid.Value.kReverse); //front out
		} else if (leftJoy.getRawButton(4)) {
			climbfront.set(DoubleSolenoid.Value.kReverse); //front in
		} else {
			climbfront.set(DoubleSolenoid.Value.kOff);
		}

		if (rightJoy.getRawButton(5)) {
			climbback.set(DoubleSolenoid.Value.kForward); // backout
		} else if (rightJoy.getRawButton(4)) {
			climbfront.set(DoubleSolenoid.Value.kForward); // back in
		} else {
			climbback.set(DoubleSolenoid.Value.kOff);
		}
	}*/ 
	
	public void followLine() {
	    if (color1 < COLOR_TAPE_VALUE && color2 < COLOR_TAPE_VALUE){
            setMotors(MOTOR_SPEED_TAPE, MOTOR_SPEED_TAPE);
		}
		
	    else if (color1 < COLOR_TAPE_VALUE && color2 > COLOR_TAPE_VALUE){
            setMotors(0, TURNING_SPEED);
	    }
		
	    else if (color1 > COLOR_TAPE_VALUE && color2 < COLOR_TAPE_VALUE){ 
            setMotors(TURNING_SPEED, 0);
		}
	}
	
	
	/**
	 * Get the proportion of motor speed based on the distance to the target value.
	 * Used in auto.
	 * 
	 * @param target
	 *            the target value
	 * @param currentEnc
	 *            the current encoder value
	 * @return the proportion of the motor speed constant to set
	 */
	public static double prop(double target, double currentEnc) {
		if (((target - currentEnc) / target) > 0.8) {
			return -0.4;
		} else if (((target - currentEnc) / target) < 0.4) {
			return -0.3;
		} else {
			return (((target - currentEnc) / target) * DRIVE_CONSTANT);
		}
	}

	/**
	 * Gets the proportion to be used with motor speed during gyro turns. Used in
	 * auto.
	 * 
	 * Target should ALWAYS be positive.
	 * 
	 * @param target
	 *            the target value
	 * @param currentGyro
	 *            the current gyro value
	 * @return the proportion of the motor speed constant to set
	 */
	public static double propGyro(double target, double currentGyro) {
		if ((1 - (currentGyro / target)) <= 0.5) {
			return 1 - (currentGyro / target);
		}
		return 0.5;
	}

	/**
	 * Turns the robot to the right a specified amount. Auto method.
	 * 
	 * @param targetAngle
	 *            the angle to turn to (in degrees)
	 * @param currentAngle
	 *            the current gyro angle (in degrees)
	 */
	private void turnRight(double targetAngle, int counter) {
		// Remember that negative is forwards in auto.
		if (angle > targetAngle) {
			setMotors(0, 0);
			enc1.reset();
			counter++;
		} else {
			System.out.println(propGyro(targetAngle, angle));
			setMotors(-propGyro(targetAngle, angle), propGyro(targetAngle, angle));
		}
	}

	/**
	 * Turns the robot to the left a specified amount. Auto method.
	 * 
	 * @param targetAngle
	 *            the angle to turn to (in degrees)
	 * @param currentAngle
	 *            the current gyro angle (in degrees)
	 */
	private void turnLeft(double targetAngle, int counter) {
		// Remember that negative is forwards in auto.
		targetAngle = -targetAngle;
		if (angle < targetAngle) {
			setMotors(0, 0);
			enc1.reset();
			counter++;
		} else {
			System.out.println(propGyro(targetAngle, angle));
			setMotors(propGyro(targetAngle, angle), -propGyro(targetAngle, angle));	
		}
	}

	/**
	 * Moves the robot forwards a specified distance (in feet). Auto method.
	 * 
	 * @param targetDistance
	 *            the distance to go (feet)
	 * @param currentDistance
	 *            the current distance (feet)
	 */
	private void moveForward(double targetDistance, int counter) {
		if (dist >= targetDistance) {
			setMotors(0, 0);
			enc1.reset();
			gyro.reset();
			counter++;
		} else {
			setMotors(prop(targetDistance, dist), prop(targetDistance, dist));
		}
	}

	@Override
	public void autonomousPeriodic() {
		teleopPeriodic();
	}

	/**
	 * This function is called periodically during test mode.
	 */
	@Override
	// We do not use this, as no autonomous code was needed.
	public void testPeriodic() {
		color1 = colorsensor1.getVoltage();
		color2 = colorsensor2.getVoltage();
		if (color1 == COLOR_TAPE_VALUE || color2 == COLOR_TAPE_VALUE) {
			followLine();
		}
	}
}
